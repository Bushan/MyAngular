var app = angular.module('test', []);


app.controller("index", function($scope, $http) {
	
	//Fetch properties file
//	$http.get('resources/label.properties').success(data){
//		$scope.labels=data;
//	}

	//Fetch data from json file
	$http.get('data/data.json').success(function(data){
		$scope.student=data.student;
		
	});
	
	//Add the new details into the grid.
	$scope.addPerson = function() {
		console.log("This application is now saving...");
		
		$scope.student.push({name:$scope.name,age:$scope.age,title:$scope.title});
		console.log("Name is .. " + JSON.stringify($scope.student));
		console.log("testing method...");
		
		$scope.name='';
		$scope.age='';
		$scope.title='';
	};

	//Remove the entry from the grid.
	$scope.remove = function(index) {
		$scope.student.splice(index, 1);
	};
	
	//Save the new details into Json
	$scope.save=function(){
		
	};

});
